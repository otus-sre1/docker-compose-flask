import unittest
import requests


class FlaskAppTestCase(unittest.TestCase):

    def test_home_page(self):
        """Testing the main page."""
        response = requests.get('http://localhost')
        self.assertEqual(response.status_code, 200)
        # print (response.content)

    def test_invalid_page(self):
        """Testing an invalid page."""
        response = requests.get('http://localhost/invalid')
        self.assertEqual(response.status_code, 404)
        # print (response.content)


if __name__ == '__main__':
    unittest.main()
