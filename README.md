# docker-compose-flask

```

+-------------+       +------------+         +--------------+     +-----------+
|             |       |            |         |              |     |           |
|    nginx    +-------+  gunicorn  +---------+  flask app   +-----+   redis   |
|             |       |            |         |              |     |           |
+-------------+       +------------+         +--------------+     +-----------+

```

## Environments setup

please install `docker` and `docker-compose`.

```sh
$ docker version
Client:
 Version:	17.12.0-ce
 API version:	1.35
 Go version:	go1.9.2
 Git commit:	c97c6d6
 Built:	Wed Dec 27 20:03:51 2017
 OS/Arch:	darwin/amd64

Server:
 Engine:
  Version:	17.12.0-ce
  API version:	1.35 (minimum version 1.12)
  Go version:	go1.9.2
  Git commit:	c97c6d6
  Built:	Wed Dec 27 20:12:29 2017
  OS/Arch:	linux/amd64
  Experimental:	true
$ docker-compose version
docker-compose version 1.18.0, build 8dd22a9
docker-py version: 2.6.1
CPython version: 2.7.12
OpenSSL version: OpenSSL 1.0.2j  26 Sep 2016
```

## Quick start

### build and up

```sh
$ git clone https://github.com/xiaopeng163/docker-compose-flask
$ cd docker-compose-flask
$ docker-compose build
$ docker-compose up -d
Starting dockercomposeflask_redis_1 ... done
Starting dockercomposeflask_web_1 ... done
Starting dockercomposeflask_nginx_1 ... done
```

Check the service running information

```sh
$ docker-compose ps
           Name                         Command               State           Ports
--------------------------------------------------------------------------------------------
dockercomposeflask_nginx_1   /usr/sbin/nginx                  Up      0.0.0.0:80->80/tcp
dockercomposeflask_redis_1   docker-entrypoint.sh redis ...   Up      6379/tcp
dockercomposeflask_web_1     /runserver.sh                    Up      0.0.0.0:8000->8000/tcp
```

Check the web service

```sh
$ curl 127.0.0.1
Hello Container World! I have been seen 1 times and my hostname is 09ad15ad1b51.
$ curl 127.0.0.1
Hello Container World! I have been seen 2 times and my hostname is 09ad15ad1b51.
$ curl 127.0.0.1
Hello Container World! I have been seen 3 times and my hostname is 09ad15ad1b51.
```

### stop the service

```sh
$ docker-compose stop
Stopping dockercomposeflask_nginx_1 ... done
Stopping dockercomposeflask_web_1   ... done
Stopping dockercomposeflask_redis_1 ... done
```

## Выполнение задания

### Добавлен [код unittest ответа web сервера](https://gitlab.com/otus-sre1/docker-compose-flask/-/blob/master/tests/unit/status_code.py)

### Выполним unittest на работающем web сервере

```shell
python3 tests/unit/status_code.py
..
----------------------------------------------------------------------
Ran 2 tests in 0.032s

OK
```

### Остановим web сервер и выполним unittest

```shell
docker compose ps -a                          
NAME                           IMAGE                        COMMAND                  SERVICE             CREATED              STATUS                       PORTS
docker-compose-flask-nginx-1   docker-compose-flask-nginx   "/docker-entrypoint.…"   nginx               About a minute ago   Up About a minute            0.0.0.0:80->80/tcp
docker-compose-flask-redis-1   redis:buster                 "docker-entrypoint.s…"   redis               About a minute ago   Up About a minute            6379/tcp
docker-compose-flask-web-1     docker-compose-flask-web     "/runserver.sh"          web                 About a minute ago   Exited (137) 5 seconds ago
```

```shell
python3 tests/unit/status_code.py             
FF
======================================================================
FAIL: test_home_page (__main__.FlaskAppTestCase.test_home_page)
Testing the main page.
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/podkovka/Documents/SRE/docker-compose-flask/tests/unit/status_code.py", line 10, in test_home_page
    self.assertEqual(response.status_code, 200)
AssertionError: 502 != 200

======================================================================
FAIL: test_invalid_page (__main__.FlaskAppTestCase.test_invalid_page)
Testing an invalid page.
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/podkovka/Documents/SRE/docker-compose-flask/tests/unit/status_code.py", line 16, in test_invalid_page
    self.assertEqual(response.status_code, 404)
AssertionError: 502 != 404

----------------------------------------------------------------------
Ran 2 tests in 6.189s

FAILED (failures=2)
```

### Добавим [healthcheck для всех сервисов](https://gitlab.com/otus-sre1/docker-compose-flask/-/commit/c472d05127168e0d9742ad54b24322e8741807ca)

```shell
docker compose ps                                             
NAME                           IMAGE                        COMMAND                  SERVICE             CREATED             STATUS                   PORTS
docker-compose-flask-nginx-1   docker-compose-flask-nginx   "/docker-entrypoint.…"   nginx               10 seconds ago      Up 8 seconds (healthy)   0.0.0.0:80->80/tcp
docker-compose-flask-redis-1   redis:buster                 "docker-entrypoint.s…"   redis               10 seconds ago      Up 9 seconds (healthy)   6379/tcp
docker-compose-flask-web-1-1   docker-compose-flask-web-1   "/runserver.sh"          web-1               10 seconds ago      Up 9 seconds (healthy)   
docker-compose-flask-web-2-1   docker-compose-flask-web-2   "/runserver.sh"          web-2               10 seconds ago      Up 9 seconds (healthy)
```

```shell
docker inspect docker-compose-flask-redis-1
...
"Health": {
                "Status": "healthy",
                "FailingStreak": 0,
                "Log": [
                    {
                        "Start": "2023-07-03T07:31:21.859409334Z",
                        "End": "2023-07-03T07:31:21.93210775Z",
                        "ExitCode": 0,
                        "Output": "PONG\n"
                    },
....
```

### Настройка отказоустойчивости web-сервера

[Добавим в стек еще один web-сервер](https://gitlab.com/otus-sre1/docker-compose-flask/-/blob/master/docker-compose.yml)

[Внесем изменения в конфигурацию nginx](https://gitlab.com/otus-sre1/docker-compose-flask/-/blob/master/nginx/sites-enabled/app)

Запускаем стек

```shell
docker compose up -d --build
....
✔ Network docker-compose-flask_default    Created 
✔ Container docker-compose-flask-redis-1  Started
✔ Container docker-compose-flask-nginx-1  Started
✔ Container docker-compose-flask-web-2-1  Started
✔ Container docker-compose-flask-web-1-1  Started
```

Проверяем работоспособность

```shell
for (( i=1; i <= 10; i++ )); do
curl http://localhost/
done
Hello Container World! I have been seen b'1' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'2' times and my hostname is 90f167259a52.
Hello Container World! I have been seen b'3' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'4' times and my hostname is 90f167259a52.
Hello Container World! I have been seen b'5' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'6' times and my hostname is 90f167259a52.
Hello Container World! I have been seen b'7' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'8' times and my hostname is 90f167259a52.
Hello Container World! I have been seen b'9' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'10' times and my hostname is 90f167259a52.
```

Выключаем один web-сервер

```shell
docker compose stop web-2   
[+] Stopping 1/1
✔ Container docker-compose-flask-web-2-1  Stopped
```

Проверяем работоспосоность

```shell
for (( i=1; i <= 10; i++ )); do
curl http://localhost/
done
Hello Container World! I have been seen b'11' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'12' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'13' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'14' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'15' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'16' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'17' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'18' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'19' times and my hostname is b95fbb5e183e.
Hello Container World! I have been seen b'20' times and my hostname is b95fbb5e183e.
```
